from quart import current_app as app
async def get_travel_time(line, start_station, f_station):
    schedule = app.db["schedules"][line]
    total_time = 0
    for station in schedule["forward"]:
        if start_station in station["station"] and station["time"] != "-":
            total_time -= int(station["time"])
        elif f_station in station["station"]:
            total_time += int(station["time"])
    if total_time < 0:
        total_time = 0
        for station in schedule["backwards"]:
            if start_station in station["station"] and station["time"] != "-":
                total_time -= int(station["time"])
            elif f_station in station["station"]:
                total_time += int(station["time"])
    return total_time
